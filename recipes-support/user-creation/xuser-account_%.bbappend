# look for files in the layer first
FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

USERADD_PARAM:${PN} = "--create-home \
                       --groups audio,video,tty,input,shutdown,disk \
                       --user-group xuser"
