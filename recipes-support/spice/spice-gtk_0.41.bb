SUMMARY = "SPICE Client"
LICENSE = "LGPL-2.1-or-later"
DEPENDS = "celt051 cyrus-sasl gtk+3 jpeg pulseaudio zlib libusb orc gstreamer1.0 \
           gstreamer1.0-plugins-base spice-protocol \
           python3-six-native python3-pyparsing-native glib-2.0 json-glib libopus lz4"
LIC_FILES_CHKSUM = "file://COPYING;md5=4fbd65380cdd255951079008b364516c"

inherit meson gettext pkgconfig python3-dir python3native

EXTRA_OEMESON += " \
                 -Dcoroutine=gthread \
                 -Dintrospection=disabled \
                 -Dgtk_doc=disabled \
                 -Dsmartcard=disabled \
"

SRC_URI = "http://spice-space.org/download/gtk/spice-gtk-${PV}.tar.xz \
           ${@bb.utils.contains('DISTRO_FEATURES', 'opengl', '', 'file://disable-epoxy.patch', d)}"

PACKAGES += "${PN}-vala"
FILES:${PN}-vala += "${datadir}/vala/*"

SRC_URI[sha256sum] = "d8f8b5cbea9184702eeb8cc276a67d72acdb6e36e7c73349fb8445e5bca0969f"
