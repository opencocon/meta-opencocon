require nx-libs.inc

DEPENDS += "nxcomp"
S = "${WORKDIR}/git/${PN}"

SRC_URI += " \
             file://fix-header.patch \
             file://nxproxy-makefile-am.patch \
"
