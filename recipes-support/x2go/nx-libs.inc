HOMEPAGE = "http://wiki.x2go.org"
LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://../COPYING;md5=751419260aa954499f7abaabaa882bbe"
SRC_URI = "git://code.x2go.org/nx-libs.git;protocol=git;branch=master"
SRCREV = "8129c1f6bea9eac2e7af067414ebcf47c9e2fe51"
inherit autotools
