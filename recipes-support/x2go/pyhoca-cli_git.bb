HOMEPAGE = "http://wiki.x2go.org"
LICENSE = "AGPL-3.0-or-later"
LIC_FILES_CHKSUM = "file://COPYING;md5=73f1eb20517c55bf9493b7dd6e480788"

SRC_URI = "git://code.x2go.org/pyhoca-cli.git;protocol=https;branch=master \
"
SRCREV = "7303ada0a83b70863b1805452288919e8efdc235"

inherit setuptools3

RDEPENDS:${PN} = "nxproxy python3-x2go python3-setproctitle"
