require nx-libs.inc

DEPENDS += "zlib libpng libjpeg-turbo xorgproto pkgconfig-native"
S = "${WORKDIR}/git/${PN}"

SRC_URI:append = "\
                  file://nxcomp-disable-test.patch"

do_configure:prepend() {
  # configure checks VERSION file, so copy to build directory.
  cp ${S}/VERSION ${B}/
}
