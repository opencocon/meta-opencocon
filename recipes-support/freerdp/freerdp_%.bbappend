# look for files in the layer first
FILESEXTRAPATH:prepend := "${THISDIR}/${PN}:"

DEPENDS:append = " \
                   cairo \
"
EXTRA_OECMAKE:append = " \
                         -DWITH_CAIRO=ON \
"
