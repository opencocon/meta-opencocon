DESCRIPTION = "TUI Mastodon Client"
HOMEPAGE = "https://github.com/taka-tuos/nanotodon"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=64c7a8874eef0034fa2670c55a5841d0"

SRCREV = "5091e6d8e9c0c2a103e8557c08bf3ff2b3fb5c88"
SRC_URI = "git://github.com/taka-tuos/nanotodon.git;branch=master;protocol=https \
	   file://err_png.patch \
"

DEPENDS = "curl ca-certificates libwebp"
S = "${WORKDIR}/git"

do_compile() {
        CFLAGS="${CFLAGS} -DUSE_SIXEL -DUSE_WEBP" LDFLAGS="${LDFLAGS} -lwebp" CC="${CC}"  oe_runmake 
}

do_install() {
        install -d ${D}${bindir}/
        install -m 0755    ${S}/nanotodon     ${D}${bindir}/nanotodon
        install -d ${D}${datadir}/pixmaps/nanotodon/
        install -m 0644    ${S}/err.png     ${D}${datadir}/pixmaps/nanotodon/err.png
}

