DESCRIPTION = "multi-platform screen and application forwarding system"
HOMEPAGE = "http://www.xpra.org/src/"
LICENSE = "GPL-2.0-or-later"
LIC_FILES_CHKSUM = "file://COPYING;md5=751419260aa954499f7abaabaa882bbe"

SRC_URI = "https://files.pythonhosted.org/packages/source/x/xpra/${BPN}-${PV}.tar.gz \
           file://no_use_bash.patch \
           file://xpra-4.4.1-maxPixelClock-long-long.patch \
           file://xpra-4.4.1-no-pandoc.patch \
"

DEPENDS = "python3-cython-native libx11 libxrandr libxtst libxfixes libxcomposite libxdamage \
           gtk+3 libxext libxkbfile python3-pygobject libvpx jpeg libwebp pkgconfig-native libxres lz4 \
           ${@bb.utils.contains('DISTRO_FEATURES', 'opengl', 'mesa drm', '', d)}"

RDEPENDS:${PN} = "python3-rencode python3-pillow"

# client only (for OpenCocon distribution)
DISTUTILS_BUILD_ARGS := "--without-html5 --without-minify --without-html5_gzip \
                        --without-html5_brotli --without-example \
                        --without-service --without-cuda_kernels \
                        --without-cuda_rebuild --with-vpx \
                        --without-jpeg_encoder --without-v4l2 \
                        --without-server --without-printing \
                        ${@bb.utils.contains('DISTRO_FEATURES', 'opengl', '--with-opengl --with-drm', '--without-opengl --without-drm', d)}"

PACKAGES += " \
  ${PN}-server \
"

FILES:${PN} += "\
  ${datadir}/appdata/* \
  ${datadir}/applications/* \
  ${datadir}/etc/xpra/* \
  ${datadir}/etc/X11/* \
  ${datadir}/etc/dbus-1/system.d/* \
  ${datadir}/icons/* \
  ${datadir}/mime/packages/* \
  ${datadir}/lib/udev/rules.d/* \
  ${datadir}/lib/xpra/* \
  ${datadir}/lib/tmpfiles.d/* \
  ${datadir}/lib/sysusers.d/* \
  ${datadir}/lib/cups/backend/* \
  ${datadir}/libexec/* \
  ${datadir}/metainfo/* \
"

FILES:${PN}-server += "\
  ${datadir}/lib/systemd/system/* \
  ${datadir}/etc/default/xpra \
  ${datadir}/etc/pam.d/xpra \
"

SRC_URI[sha256sum] = "c1c5f8aa26bae7a77b0f52998c4178f5d045cf425ec78d007a92c145bfe025e8"

inherit setuptools3_legacy mime-xdg mime
