FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

# for support xpm image
GDK_PIXBUF_LOADERS = "png jpeg others"
PACKAGECONFIG[others] = "-Dothers=enabled,-Dothers=disabled"
