#!/bin/bash


usage() {
  echo "Usage: $(basename "$0") [options]"
  echo " -C ---- create ISO CD image"
  echo " -d (string) ---- Distribution name [opencocon, kitsuneko def:opencocon]"
  echo " -D (path) ---- Source download directory [$(pwd)/cocon-downloads]"
  echo " -l (string) ---- Target libc name [glibc, musl def:musl]"
  echo " -m (string) ---- Target machine name [cocon486, cocon686 def:cocon486]"
  echo " -n (string) ---- Release name"
  echo " -o (dir) ---- Output directory [$(pwd)/cocon-outputs]"
  echo " -r (path) ---- Downloaded repo reposirory (required)"
  echo " -R (string) ---- OpenEmbedded/Yocto release name"
  echo "   Hint : To build OpenCocon v11, input\"kirkstone\"."
  echo " -w (dir) ---- Work directory [$(pwd)/cocon-builds]"
  echo ""
  echo "This script is for auto-build and upload OpenCocon/Kitsuneko."
  exit 1
}

# デフォルトパラメータを設定
COCONBUILD_TARGET_MACHINE="cocon486"
COCONBUILD_DISTRO_NAME="opencocon"
COCONBUILD_YOCTO_VERSION="kirkstone"
COCONBUILD_LIBC="musl"
COCONBUILD_DOWNLOAD_DIR="$(pwd)/cocon-downloads"
COCONBUILD_OUTPUT_DIR="$(pwd)/cocon-outputs"
COCONBUILD_WORKDIR="$(pwd)/cocon-builds"

# make and check Parameter
RELEASE_NAME="snapshot-$( date +"%Y%m%d" )"

while getopts 'Cd:D:l:m:n:o:r:R:w:' OPTION; do
  case "$OPTION" in

  C)
    COCONBUILD_CREATE_CDIMAGE=1 ;;

  d)
    COCONBUILD_DISTRO_NAME="$OPTARG" ;;

  D)
    COCONBUILD_DOWNLOAD_DIR="$OPTARG" ;;

  l)
    COCONBUILD_LIBC="$OPTARG" ;;

  m)
    COCONBUILD_TARGET_MACHINE="$OPTARG" ;;

  n)
    RELEASE_NAME="$OPTARG" ;;

  o)
    COCONBUILD_OUTPUT_DIR="$OPTARG" ;;

  r)
    # get full path
    COCONBUILD_REPO_DIR="$(realpath "$OPTARG")" ;;

  R)
    COCONBUILD_YOCTO_VERSION="$OPTARG" ;;

  w)
    COCONBUILD_WORKDIR="$OPTARG" ;;

  ?)
    usage ;;

  esac
done

# make and check Parameter
CD_VERSION="${RELEASE_NAME}"

if [ -z "$COCONBUILD_REPO_DIR" ];
then
  echo "Error : Please specify Source repository directory : -r (dir)."
  usage
fi


if [ "$COCONBUILD_DISTRO_NAME" = "opencocon" ];
then
  COCONBUILD_IMAGE_RECIPE=(opencocon-image)
  COCONBUILD_INITRAMFS_RECIPE="initramfs-crusoe-image"

  if [ "$COCONBUILD_TARGET_MACHINE" = "cocon486" ];
  then
    COCONBUILD_PICKUP_FILES="bzImage crusoe-cocon486.squashfs opencocon-cocon486.pcbios-hdimg.gz opencocon-cocon486.pcbios-hdimg.vdi.xz opencocon-cocon486.squashfs opencocon-cocon486.tar.gz"
  elif [ "$COCONBUILD_TARGET_MACHINE" = "cocon686" ];
  then
    # TODO
    COCONBUILD_PICKUP_FILES="bzImage crusoe-cocon686.squashfs opencocon-cocon686.pcbios-hdimg.gz opencocon-cocon686.pcbios-hdimg.vdi.xz opencocon-cocon686.squashfs opencocon-cocon686.tar.gz"
  else
    echo "Error: Unsupported target."
    usage
  fi 

elif [ "$COCONBUILD_DISTRO_NAME" = "kitsuneko" ];
then
  COCONBUILD_IMAGE_RECIPE=(kitsuneko-image)
  COCONBUILD_INITRAMFS_RECIPE="initramfs-crusoe-image"

  if [ "$COCONBUILD_TARGET_MACHINE" = "cocon486" ];
  then
    COCONBUILD_PICKUP_FILES="bzImage kitsuneko-image-cocon486.pcbios-hdimg.gz kitsuneko-image-cocon486.pcbios-hdimg.vdi.xz kitsuneko-image-cocon486.tar.gz"
  elif [ "$COCONBUILD_TARGET_MACHINE" = "cocon686" ];
  then
    COCONBUILD_PICKUP_FILES="bzImage kitsuneko-image-cocon686.pcbios-hdimg.gz kitsuneko-image-cocon686.pcbios-hdimg.vdi.xz kitsuneko-image-cocon686.tar.gz"
  else
    echo "Error: Unsupported target."
    usage
  fi 

elif [ "$COCONBUILD_DISTRO_NAME" = "both" ];
then
  COCONBUILD_IMAGE_RECIPE=(opencocon-image kitsuneko-image)
  COCONBUILD_INITRAMFS_RECIPE="initramfs-crusoe-image"

  if [ "$COCONBUILD_TARGET_MACHINE" = "cocon486" ];
  then
    COCONBUILD_PICKUP_FILES="bzImage crusoe-cocon486.squashfs kitsuneko-image-cocon486.pcbios-hdimg.gz kitsuneko-image-cocon486.pcbios-hdimg.vdi.xz kitsuneko-image-cocon486.tar.gz opencocon-cocon486.pcbios-hdimg.gz opencocon-cocon486.pcbios-hdimg.vdi.xz opencocon-cocon486.squashfs opencocon-cocon486.tar.gz"
  elif [ "$COCONBUILD_TARGET_MACHINE" = "cocon686" ];
  then
    COCONBUILD_PICKUP_FILES="bzImage crusoe-cocon686.squashfs kitsuneko-image-cocon686.pcbios-hdimg.gz kitsuneko-image-cocon686.pcbios-hdimg.vdi.xz kitsuneko-image-cocon686.tar.gz opencocon-cocon686.pcbios-hdimg.gz opencocon-cocon686.pcbios-hdimg.vdi.xz opencocon-cocon686.squashfs opencocon-cocon686.tar.gz"
  else
    echo "Error: Unsupported target."
    usage
  fi 



else
  echo "Error: Unsupported distribution name."
  usage
fi

# Check nesesary program

## To compile for cocon486 and cocon686, needs 32bit gcc.
## TODO : もっとベターな確認方法があるように思う。あとruntimeの方が本当はよい。
#if [ ! "$(command -v i686-linux-gnu-ld)" ];
#then
#  echo "Error: 32-bit build tools is not found. please install gcc-multilib"
#  exit 2
#fi

## zset, needed since honister.
if [ ! "$(command -v zstd)" ];
then
  echo "Error: zstd is not found. please install this."
  exit 2
fi

## wget
if [ ! "$(command -v wget)" ];
then
  echo "Error: wget is not found. please install this."
  exit 2
fi

# Show status
echo "Distribution name : ${COCONBUILD_DISTRO_NAME}"
echo "Base OE/Yocto Release : ${COCONBUILD_YOCTO_VERSION}"
echo "Target machine : ${COCONBUILD_TARGET_MACHINE}"
echo "Target libc : ${COCONBUILD_LIBC}"
echo "Repository directory : ${COCONBUILD_REPO_DIR}"
echo "Source dowwnload directory : ${COCONBUILD_DOWNLOAD_DIR}"
echo "Work directory : ${COCONBUILD_WORKDIR}"
echo "Output Directory : ${COCONBUILD_OUTPUT_DIR}"
echo "Create ISO image flag : ${COCONBUILD_CREATE_CDIMAGE}"
echo "Pickup image files : ${COCONBUILD_PICKUP_FILES}"
if [ "$COCONBUILD_CREATE_CDIMAGE" -eq 1 ];
then
  echo "ISO image version : ${CD_VERSION}"
fi

# Check deploy directory exists
if [ ! -d "$COCONBUILD_REPO_DIR" ] || [ ! -d "${COCONBUILD_REPO_DIR}/meta-opencocon" ] || [ ! -d "${COCONBUILD_REPO_DIR}/meta-coconport" ] || [ ! -d "${COCONBUILD_REPO_DIR}/poky" ]; 
then
  echo "Error : Source repository is not found."
  exit 2
fi
 
# Check download directory exists
if [ ! -d "$COCONBUILD_DOWNLOAD_DIR" ];
then
  # make directory
  mkdir -p "$COCONBUILD_DOWNLOAD_DIR"
fi

# Check output directory exists
if [ ! -d "$COCONBUILD_OUTPUT_DIR" ];
then
  # make directory
  mkdir -p "$COCONBUILD_OUTPUT_DIR"
fi

# Check work directory exists
if [ ! -d "$COCONBUILD_WORKDIR" ];
then
  # make directory
  mkdir -p "$COCONBUILD_WORKDIR"
fi
 
# If not download syslinux-4.07, do download on this.
if [ "$COCONBUILD_CREATE_CDIMAGE" -eq 1 ] && [ "$COCONBUILD_DISTRO_NAME" = "opencocon" ] || [ "$COCONBUILD_DISTRO_NAME" = "both" ];
then
  if [ ! -e "${COCONBUILD_WORKDIR}/syslinux-4.07.tar.bz2" ];
  then
    echo "Trying to download syslinux-4.07."
    # Trying Download syslinux-4.07
    wget -O "${COCONBUILD_WORKDIR}/syslinux-4.07.tar.bz2" "https://mirrors.edge.kernel.org/pub/linux/utils/boot/syslinux/syslinux-4.07.tar.bz2"
  fi

  if [ ! -d "${COCONBUILD_WORKDIR}/syslinux-4.07/" ];
  then
    echo "Trying to extract syslinux-4.07."
    if [ "$( sha256sum "${COCONBUILD_WORKDIR}/syslinux-4.07.tar.bz2" | cut -d ' ' -f 1 )" != '1240a4e4219b518bdaef78931b6e901befeff35e6894ac6db785115848a7a05a' ];
    then
      echo "Error: checksum of syslinux-4.07 is invalid."
      exit 2
    fi
    tar -xf "${COCONBUILD_WORKDIR}/syslinux-4.07.tar.bz2" -C "${COCONBUILD_WORKDIR}/" 
  fi
fi

# ビルド環境の workspace を作る
cd "$COCONBUILD_REPO_DIR" || { echo "Error: cd fail."; exit 3; }

# shellcheck source=/dev/null
source meta-opencocon/scripts/opencoconsetup.sh -f -m "$COCONBUILD_TARGET_MACHINE" -b "$COCONBUILD_WORKDIR"
echo "build directory : $( pwd )"

# downloadディレクトリの整備をする
if [ ! -h "$COCONBUILD_DOWNLOAD_DIR" ];
then
  ln -sf "$COCONBUILD_DOWNLOAD_DIR" downloads
fi

# Set conf/local.conf
# first, remove previous customized conf/local.conf if exists.
if [ ! -e conf/local.conf.org ];
then
  cp conf/local.conf conf/local.conf.org
fi
rm conf/local.conf

sed -e '/^DISTRO_VERSION/d' < conf/local.conf.org | sed -e '/^DISTRO =/d' > conf/local.conf
{
  #echo "DISTRO = \"${COCONBUILD_DISTRO_NAME}\""
  echo "DISTRO = \"opencocon\""
  echo "DISTRO_VERSION = \"${RELEASE_NAME}\""
  echo "TCLIBC = \"${COCONBUILD_LIBC}\""
} >> conf/local.conf


# first, cleanup old image
bitbake -c cleanall "${COCONBUILD_IMAGE_RECIPE[@]}" "$COCONBUILD_INITRAMFS_RECIPE"
BITBAKE_STAT=$?

# fetch bitbake error
if [ "$BITBAKE_STAT" -ne 0 ];
then
  echo "build error $BITBAKE_STAT"

  exit "$BITBAKE_STAT"
fi

# run bitbake
bitbake "${COCONBUILD_IMAGE_RECIPE[@]}" "$COCONBUILD_INITRAMFS_RECIPE"
BITBAKE_STAT=$?

# fetch bitbake error
if [ "$BITBAKE_STAT" -ne 0 ];
then
  echo "build error $BITBAKE_STAT"

  exit "$BITBAKE_STAT"
fi

# Copy created image files to output directory
echo -n "${COCONBUILD_PICKUP_FILES}" | xargs -d ' ' -I {} rm "${COCONBUILD_OUTPUT_DIR}/{}" 2>/dev/null
echo -n "${COCONBUILD_PICKUP_FILES}" | xargs -d ' ' -I {} cp "${COCONBUILD_WORKDIR}/tmp-${COCONBUILD_LIBC}/deploy/images/${COCONBUILD_TARGET_MACHINE}/{}" "${COCONBUILD_OUTPUT_DIR}/"

# If COCONBUILD_CREATE_CDIMAGE is on, make CD image.
if [ "$COCONBUILD_CREATE_CDIMAGE" -eq 1 ];
then
  if [ "$COCONBUILD_DISTRO_NAME" = "opencocon" ] || [ "$COCONBUILD_DISTRO_NAME" = "both" ];
  then
    echo "Make CD image"

    # first, cleanup work directory.
    if [ -d "${COCONBUILD_WORKDIR}/coconcd-work/" ];
    then
      rm -rf "${COCONBUILD_WORKDIR}/coconcd-work/"
    fi

    # Set Ramdisk size on CD boot
    if [ "$COCONBUILD_TARGET_MACHINE" = "cocon486" ] || [ "$COCONBUILD_TARGET_MACHINE" = "cocon686" ];
    then
      # Do make CD image
      "$COCONBUILD_REPO_DIR"/meta-coconport/x86-common/scripts/cdcreator-syslinux4.sh -d "opencocon" -l "$COCONBUILD_LIBC" -m "$COCONBUILD_TARGET_MACHINE" -n "$CD_VERSION" -S "${COCONBUILD_WORKDIR}/syslinux-4.07/" -c "${COCONBUILD_REPO_DIR}/meta-coconport/x86-common/scripts/coconcd/" -i "tmp-${COCONBUILD_LIBC}/deploy/images/${COCONBUILD_TARGET_MACHINE}/" -w "${COCONBUILD_WORKDIR}/coconcd-work/"  -o "$COCONBUILD_OUTPUT_DIR"
    fi
  fi
fi


exit 0
