DESCRIPTION = "bubblemon dockapp"
HOMEPAGE = "https://github.com/rnjacobs/wmbubble/"
LICENSE = "GPL-2.0-or-later"
LIC_FILES_CHKSUM = "file://bubblemon.c;beginline=6;endline=42;md5=65bf6a239ab5bdd4d022c0eab4097db6"

SRCREV = "f43a37e76eed1b0017ebbe19db0050ba8750a268"
SRC_URI = "git://github.com/rnjacobs/wmbubble.git;branch=master;protocol=https \
"

SRC_URI:append:libc-musl = "file://musl.patch"

DEPENDS = "xorgproto libx11"
S = "${WORKDIR}/git"
EXTRA_CFLAGS ?= "${CFLAGS}"

do_configure:append() {
	sed -i -e "/^CC = gcc/d" ${S}/Makefile
	sed -i -e "1i CC = ${TARGET_PREFIX}gcc ${TOOLCHAIN_OPTIONS} ${EXTRA_CFLAGS}" ${S}/Makefile

	sed -i -e "s/^play /aplay /g" ${S}/misc/wakwak.sh
}

do_compile() {
        oe_runmake 
}

do_install() {
        install -d ${D}${bindir}/
        install -m 0755    ${S}/wmbubble     ${D}${bindir}/wmbubble
        install -d ${D}${datadir}/${PN}/
        install -m 0655    ${S}/misc/wak.wav     ${D}${datadir}/${PN}/wak.wav
	install -m 0755    ${S}/misc/wakwak.sh     ${D}${datadir}/${PN}/wakwak.sh
}
