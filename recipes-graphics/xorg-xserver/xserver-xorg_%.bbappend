# look for files in the layer first
FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"


EXTRA_OEMESON += " \
                  -Dxephyr=true \
"

LIB_DEPS += "libx11 libxv xcb-util-image xcb-util-keysyms xcb-util-wm xcb-util-renderutil"
