FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI:append:libc-musl = " \
    file://10-preload-modules.conf \
"

do_install:append:libc-musl () {
    install -d ${D}/${sysconfdir}/X11/xorg.conf.d/
    install -m 0644 ${WORKDIR}/10-preload-modules.conf ${D}/${sysconfdir}/X11/xorg.conf.d/
}

FILES:${PN}:append:libc-musl = " ${sysconfdir}/X11/xorg.conf.d/*"
