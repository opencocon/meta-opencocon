DESCRIPTION = "Swimming around with sharks, dolphins and whales."
HOMEPAGE = "https://www.opengl.org/archives/resources/code/samples/glut_examples/demos/demos.html"
LICENSE = "SGI-1"
LIC_FILES_CHKSUM = "file://atlantis.c;beginline=2;endline=38;md5=8d48b55ab9555a38bb0ff0a100330e61"

SRC_URI = "https://www.opengl.org/archives/resources/code/samples/glut_examples/demos/atlantis.zip \
           file://Makefile.linux \
"

DEPENDS = "xorgproto libx11 freeglut mesa"
S = "${WORKDIR}/atlantis"

do_configure() {
        cp ${WORKDIR}/Makefile.linux ${S}/

	sed -i -e "/^CC = /d" ${S}/Makefile.linux
	sed -i -e "1i CC = ${TARGET_PREFIX}gcc ${TOOLCHAIN_OPTIONS} ${CFLAGS}" ${S}/Makefile.linux

	sed -i -e "/^LDFLAGS = /d" ${S}/Makefile.linux
	sed -i -e "1i LDFLAGS = -lglut -lGLU -lGL -lXmu -lXi -lXext -lX11 -lm ${LDFLAGS}" ${S}/Makefile.linux
}

do_compile() {
        oe_runmake -f Makefile.linux
}

do_install() {
        install -d ${D}${bindir}/
        install -m 0755    ${S}/atlantis     ${D}${bindir}/atlantis
}

SRC_URI[sha256sum] = "43bc8892085e483d5844464bb6f5e41181a547d7743a016e9baee8e061cf27f1"
