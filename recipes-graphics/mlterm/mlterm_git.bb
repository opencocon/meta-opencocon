DESCRIPTION = "MLTERM - Multi Lingual TERMinal emulator"
LICENSE = "BSD-3-Clause"

DEPENDS += "freetype fontconfig libx11 glib-2.0 glib-2.0-native intltool-native gettext gdk-pixbuf libxft libxext"

inherit autotools pkgconfig

SRC_URI = "\
	 git://github.com/arakiken/mlterm.git;protocol=https;branch=master \
         file://0001-fix-warning-when-compile-on-Yocto.patch \
"

LIC_FILES_CHKSUM = "file://LICENCE;md5=d0153aab02baf20d6127f085a57c079b"

SRCREV = "c902e3a7e2e6a81692dd2670f8d20d75bf79142c"

S = "${WORKDIR}/git"


EXTRA_OECONF += "--with-gui=xlib --with-type-engines=xft --with-imagelib=gdk-pixbuf --enable-debug --without-gtk"

FILES:${PN}-dev = ""
FILES:${PN} += "${libdir}/libmlterm_core.so \
                ${libdir}/libpobl.so.* \
                ${libdir}/libmef.so.* \
                ${libdir}/mef/libmef_*.so \
"
FILES:${PN}-dev = "${libdir}/libpobl.so \
                   ${libdir}/libmef.so \
"
FILES:${PN}-staticdev += "${libdir}/mef/libmef_*.a"

