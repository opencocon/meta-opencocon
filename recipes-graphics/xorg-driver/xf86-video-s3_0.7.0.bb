require recipes-graphics/xorg-driver/xorg-driver-video.inc

DESCRIPTION = "X.Org X server -- legacy S3 display driver"

LIC_FILES_CHKSUM = "file://COPYING;md5=0eae1e9f9b6904bf113c02c911019b1a"

SRC_URI[sha256sum] = "cf12541557d88107364895273e384f97836b428eeb1e54ed66511d69639562c8"
