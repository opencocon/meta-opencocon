require recipes-graphics/xorg-driver/xorg-driver-video.inc

DESCRIPTION = "X11 driver supporting cards based on the V1000 or the V2x00."

LIC_FILES_CHKSUM = "file://COPYING;md5=1f82ff47b53d054af9757517c438fabb"

# v*0002d.uc is microcode : sepalate this
PACKAGES =+ "microcode-${PN}" 
FILES:microcode-${PN} += "${libdir}/xorg/modules/v*0002d.uc"
INSANE_SKIP:microcode-${PN} = "arch"

SRC_URI = "git://gitlab.freedesktop.org/xorg/driver/xf86-video-rendition.git;protocol=https;branch=master \
          "
S = "${WORKDIR}/git"
SRCREV = "6bc366b2204fd0257703092191a81ff08bcde91e"

