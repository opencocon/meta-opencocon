require recipes-graphics/xorg-driver/xorg-driver-video.inc

DESCRIPTION = "X.Org X server -- Savage display driver"

# DEPENDS += "${@bb.utils.contains('DISTRO_FEATURES', 'opengl', 'drm', '', d)}"
# EXTRA_OECONF += "${@bb.utils.contains('DISTRO_FEATURES', 'opengl', '', '--disable-dri', d)}"

EXTRA_OECONF += " --disable-dri "

LIC_FILES_CHKSUM = "file://COPYING;md5=1f50f1289ca3b91a542a26ba5df51608"

SRC_URI = "git://gitlab.freedesktop.org/xorg/driver/xf86-video-savage.git;protocol=https;branch=master"

# Disable optimize flag
CFLAGS:append = " -O0 -fno-reorder-functions -fno-stack-protector -fno-strict-aliasing -fno-strength-reduce -fno-unroll-loops"
LDFLAGS:append = " -O0 "

S = "${WORKDIR}/git"
SRCREV = "ac67fa9515ceb9a115feb56e28b8191af886a563"
