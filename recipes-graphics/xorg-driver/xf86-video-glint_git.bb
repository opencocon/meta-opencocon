require recipes-graphics/xorg-driver/xorg-driver-video.inc

DESCRIPTION = "X.Org X server -- Glint display driver"

LIC_FILES_CHKSUM = "file://COPYING;md5=724772f5b1ac0f483904fa55275a5b85"

SRC_URI = "git://gitlab.freedesktop.org/xorg/driver/xf86-video-glint.git;protocol=https;branch=master \
"

S = "${WORKDIR}/git"
SRCREV = "59d3cc7f5b06322a4a4d95e5ed6d7f212f4df236"
