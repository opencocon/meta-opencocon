require recipes-graphics/xorg-driver/xorg-driver-video.inc

DESCRIPTION = "X.Org X server -- NV display driver"

SRC_URI = "git://gitlab.freedesktop.org/xorg/driver/xf86-video-nv.git;protocol=https;branch=master \
          "

S = "${WORKDIR}/git"
SRCREV = "1b735e8c9681dcccd54ea0295c4853763dabb8d1"

LIC_FILES_CHKSUM = "file://COPYING;md5=5f26d42045c078fef2e284111eabdd31"

