require recipes-graphics/xorg-driver/xorg-driver-video.inc

DESCRIPTION = "X.Org X server -- ark display driver"

LIC_FILES_CHKSUM = "file://COPYING;md5=bdb6aed1d4651e3c08a43c49b9b286d7"

SRC_URI = "git://gitlab.freedesktop.org/xorg/driver/xf86-video-ark.git;protocol=https;branch=master \
"

S = "${WORKDIR}/git"
SRCREV = "18a4ef933cb4de21b0663c17ddd56e0f73869679"

