require recipes-graphics/xorg-driver/xorg-driver-video.inc

DESCRIPTION = "X.Org X server -- ATI Mach64 display driver"
DEPENDS += "libxvmc ${@bb.utils.contains('DISTRO_FEATURES', 'opengl', 'drm', '', d)}"
EXTRA_OECONF += "${@bb.utils.contains('DISTRO_FEATURES', 'opengl', '', '--disable-dri', d)}"

LIC_FILES_CHKSUM = "file://COPYING;md5=be79d1b174a1e5b7e9303201e18d45f4"

SRC_URI = "git://gitlab.freedesktop.org/xorg/driver/xf86-video-mach64.git;protocol=https;branch=master \
           file://xdriver_xf86-video-mach64-cross-compile.patch"

S = "${WORKDIR}/git"
SRCREV = "74cc25c7c54bf2b2f036fd50f66f9071953db24f"
