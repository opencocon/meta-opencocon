require recipes-graphics/xorg-driver/xorg-driver-video.inc

DESCRIPTION = "X.Org X server -- Chips display driver"

LIC_FILES_CHKSUM = "file://COPYING;md5=d16ab8e6e1c8f1eaca1ef57449f284b2"

SRC_URI = "git://gitlab.freedesktop.org/xorg/driver/xf86-video-chips.git;protocol=https;branch=master"

# Disable optimize flag
CFLAGS:append = " -O0 -fno-reorder-functions -fno-stack-protector -fno-strict-aliasing -fno-strength-reduce -fno-unroll-loops"
LDFLAGS:append = " -O0 "

S = "${WORKDIR}/git"
SRCREV = "0fd51582d9a8f5f62d6e2114348143f3a3820f0a"
