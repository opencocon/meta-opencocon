require recipes-graphics/xorg-driver/xorg-driver-video.inc

DESCRIPTION = "X.org X server -- Trident display driver"

LIC_FILES_CHKSUM = "file://COPYING;md5=2e9eb6db89324a99415a93a059157da7"

SRC_URI = "git://gitlab.freedesktop.org/xorg/driver/xf86-video-trident.git;protocol=https;branch=master \
"

# Disable optimize flag
CFLAGS:append = " -O0 -fno-reorder-functions -fno-stack-protector -fno-strict-aliasing -fno-strength-reduce -fno-unroll-loops"
LDFLAGS:append = " -O0 "

S = "${WORKDIR}/git"
SRCREV = "a1d33167b9b3f8dc9adb78eade0101c2ac62f033"
