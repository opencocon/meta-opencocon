require recipes-graphics/xorg-driver/xorg-driver-video.inc

DESCRIPTION = "X11 driver for NeoMagic 2200, 2160, 2097, 2093, 2090, 2070"

LIC_FILES_CHKSUM = "file://COPYING;md5=3a6358ddf387f4be24801a5337a021a8"

SRC_URI = "git://gitlab.freedesktop.org/xorg/driver/xf86-video-neomagic.git;protocol=https;branch=master \
          "

# Disable optimize flag
CFLAGS:append = " -O0 -fno-reorder-functions -fno-stack-protector -fno-strict-aliasing -fno-strength-reduce -fno-unroll-loops"
LDFLAGS:append = " -O0 "

S = "${WORKDIR}/git"
SRCREV = "25df3ff50038ac3df4cae794b700881fb3df2bff"

