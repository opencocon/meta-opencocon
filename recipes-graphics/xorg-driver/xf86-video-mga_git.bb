require recipes-graphics/xorg-driver/xorg-driver-video.inc

DESCRIPTION = "X.Org X server -- MGA display driver"
DEPENDS += "${@bb.utils.contains('DISTRO_FEATURES', 'opengl', 'drm', '', d)}"
EXTRA_OECONF += "${@bb.utils.contains('DISTRO_FEATURES', 'opengl', '', '--disable-dri', d)}"

LIC_FILES_CHKSUM = "file://COPYING;md5=4aa220f495ce9be5ce4243d21ebac14f"

SRC_URI = "git://gitlab.freedesktop.org/xorg/driver/xf86-video-mga.git;protocol=https;branch=master \
"

S = "${WORKDIR}/git"
SRCREV = "37bf1be0fbaed7578f91485a00d37db3f1c84fe3"

