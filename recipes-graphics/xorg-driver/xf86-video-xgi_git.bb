require recipes-graphics/xorg-driver/xorg-driver-video.inc

inherit pkgconfig

DEPENDS += "drm"

LIC_FILES_CHKSUM = "file://COPYING;md5=2b1ee283c95fdba674307f121936ca9a"

SRC_URI = "git://gitlab.freedesktop.org/xorg/driver/xf86-video-xgi.git;protocol=https;branch=master \
           file://fix-configure.patch \
          "

TARGET_CFLAGS += "-fcommon"

S = "${WORKDIR}/git"
SRCREV = "979e4ce9a9b2516cc892d1776dd38e5d5260ed1b"
