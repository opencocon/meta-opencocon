require recipes-graphics/xorg-driver/xorg-driver-video.inc

DESCRIPTION = "X.Org X server -- S3 ViRGE display driver"

LIC_FILES_CHKSUM = "file://COPYING;md5=09743e0f5c076a765cd16697b5b1effb"

SRC_URI += "file://xf86-video-s3virge-Add-check-for-max-HV-Value-to-ValidMode-hook.patch"

SRC_URI = "git://gitlab.freedesktop.org/xorg/driver/xf86-video-s3virge.git;protocol=https;branch=master \
          "
S = "${WORKDIR}/git"
SRCREV = "6c66938ca97c2a24f96856c2e32010af052e9a6e"
