require recipes-graphics/xorg-driver/xorg-driver-video.inc

DESCRIPTION = "X.Org X server -- X.org SiS 671 video driver (Alternative)"
DEPENDS += "util-macros"

LIC_FILES_CHKSUM = "file://src/sis_driver.c;beginline=6;endline=28;md5=8c7057c95db5909c1e455042c48cec57"

SRC_URI = "git://github.com/rasdark/xf86-video-sis671.git;protocol=https;branch=master \
"

S = "${WORKDIR}/git"
SRCREV = "f208648902d5f969db8b69abfd0f4cfcfdfac582"

# Disable support for DRI.
EXTRA_OECONF += " --disable-dri"
