require recipes-graphics/xorg-driver/xorg-driver-video.inc

LIC_FILES_CHKSUM = "file://COPYING;md5=0b8c242f0218eea5caa949b7910a774b"

SRC_URI = "git://gitlab.freedesktop.org/xorg/driver/xf86-video-ast.git;protocol=https;branch=master" 

S = "${WORKDIR}/git"
SRCREV = "7916e2a65c53075434b4242dcafd800a5910a7e8"
