require recipes-graphics/xorg-driver/xorg-driver-video.inc

DEPENDS += "${@bb.utils.contains('DISTRO_FEATURES', 'opengl', 'drm', '', d)}"

EXTRA_OECONF += "${@bb.utils.contains('DISTRO_FEATURES', 'opengl', '', '--disable-dri', d)}"

LIC_FILES_CHKSUM = "file://COPYING;md5=41f74bf6ac6803f497df136f0896153a"

SRC_URI = "git://gitlab.freedesktop.org/xorg/driver/xf86-video-tdfx.git;protocol=https;branch=master \
"

S = "${WORKDIR}/git"
SRCREV = "af8c7ddd4805c15dfe0443c3496ab2d9bda82405"
