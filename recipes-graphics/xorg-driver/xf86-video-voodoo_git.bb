require recipes-graphics/xorg-driver/xorg-driver-video.inc

DESCRIPTION = "X11 driver for Voodoo/Voodoo2"

inherit pkgconfig

LIC_FILES_CHKSUM = "file://COPYING;md5=b2a2c00d9810cf8f963852e6362fcabc"

SRC_URI = "git://gitlab.freedesktop.org/xorg/driver/xf86-video-voodoo.git;protocol=https;branch=master \
"

S = "${WORKDIR}/git"
SRCREV = "fbde65aa7e79cb19ef044a074b36605b948d66e8"
