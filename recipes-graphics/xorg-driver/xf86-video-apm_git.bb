require recipes-graphics/xorg-driver/xorg-driver-video.inc

DESCRIPTION = "This is the Alliance Promotion driver for XFree86 4.0+"

LIC_FILES_CHKSUM = "file://COPYING;md5=0b302c1eb730ff7a191f2cbdc952f689"

SRC_URI = "git://gitlab.freedesktop.org/xorg/driver/xf86-video-apm.git;protocol=https;branch=master \
           file://ApmAccelReserveSpace.patch \
"

# Disable optimize flag
CFLAGS:append = " -O0 -fno-reorder-functions -fno-stack-protector -fno-strict-aliasing -fno-strength-reduce -fno-unroll-loops"
LDFLAGS:append = " -O0 "

S = "${WORKDIR}/git"
SRCREV = "836f8742deda9754aa379894ff33d162d0c97172"

