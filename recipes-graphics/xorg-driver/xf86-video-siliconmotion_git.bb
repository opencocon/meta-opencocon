require recipes-graphics/xorg-driver/xorg-driver-video.inc

DESCRIPTION = "X.Org X server -- SiliconMotion display driver"

LIC_FILES_CHKSUM = "file://COPYING;md5=3893e77db70569921f6d79c387b5748a"

SRC_URI = "git://gitlab.freedesktop.org/xorg/driver/xf86-video-siliconmotion.git;protocol=https;branch=master"

# Disable optimize flag
CFLAGS:append = " -O0 -fno-reorder-functions -fno-stack-protector -fno-strict-aliasing -fno-strength-reduce -fno-unroll-loops"
LDFLAGS:append = " -O0 "

S = "${WORKDIR}/git"
SRCREV = "6bb016cbec5d23de1ee531d149f987fb95a85674"

