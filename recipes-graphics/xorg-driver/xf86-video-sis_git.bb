require recipes-graphics/xorg-driver/xorg-driver-video.inc

DESCRIPTION = "X.Org X server -- SiS display driver"

# DEPENDS += "${@bb.utils.contains('DISTRO_FEATURES', 'opengl', 'drm', '', d)}"

LIC_FILES_CHKSUM = "file://COPYING;md5=cbbdd887d04deb501076c22917e2030d"

SRC_URI = "git://gitlab.freedesktop.org/xorg/driver/xf86-video-sis.git;protocol=https;branch=master"

# EXTRA_OECONF += "${@bb.utils.contains('DISTRO_FEATURES', 'opengl', '', '--disable-dri', d)}"
EXTRA_OECONF += " --disable-dri "

S = "${WORKDIR}/git"
SRCREV = "4c5ce451f0cb548a2ed66c680709050616a583f6"
