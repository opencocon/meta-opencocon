require recipes-graphics/xorg-driver/xorg-driver-video.inc

DESCRIPTION = "X.Org X server -- SiS Net2280-based USB video driver"
DEPENDS += "drm"

LIC_FILES_CHKSUM = "file://COPYING;md5=b08997efa10dc31f51dad7e85e77f182"

SRC_URI = "git://gitlab.freedesktop.org/xorg/driver/xf86-video-sisusb.git;protocol=https;branch=master"

S = "${WORKDIR}/git"
SRCREV = "922d545296e387e196836ce3c9caf48e0518e0e1"

