require recipes-graphics/xorg-driver/xorg-driver-video.inc

DESCRIPTION = "X.Org X server -- i740 display driver"

LIC_FILES_CHKSUM = "file://COPYING;md5=c85da4d100605ac6d8d47d47eb2bf191"

SRC_URI[sha256sum] = "46e401219f8ceea1faa58efd46d05a5c3f85f08d789014e7234fb755520d7a50"
