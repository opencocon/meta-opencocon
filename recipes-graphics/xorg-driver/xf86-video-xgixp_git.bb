require recipes-graphics/xorg-driver/xorg-driver-video.inc

inherit pkgconfig

DEPENDS += "drm"

LIC_FILES_CHKSUM = "file://COPYING;md5=225ca1644e92badb31a40ef755bf2364"

# Patch from buildroot
SRC_URI = "git://gitlab.freedesktop.org/xorg/driver/xf86-video-xgixp.git;protocol=https;branch=master \
           file://xdriver_xf86-video-xgixp-cross-compile.patch"

S = "${WORKDIR}/git"
SRCREV = "06ef7f688aa7dc17827d9f7723211acc5d7d76d8"

