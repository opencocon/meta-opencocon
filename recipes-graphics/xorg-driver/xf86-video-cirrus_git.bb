require recipes-graphics/xorg-driver/xorg-driver-video.inc

SUMMARY = "X.Org X server -- cirrus display driver"
DESCRIPTION = "cirrus is an Xorg driver for Cirrus Logic VGA adapters. These \
devices are not so common in the wild anymore, but QEMU can emulate one, so \
the driver is still useful."

LIC_FILES_CHKSUM = "file://COPYING;md5=6ddc7ca860dc5fd014e7f160ea699295"

SRC_URI = "git://gitlab.freedesktop.org/xorg/driver/xf86-video-cirrus.git;protocol=https;branch=master"

S = "${WORKDIR}/git"
SRCREV = "adb5a2b7503541e6cf87e4a704df7a1123ab4997"

DEPENDS += "libpciaccess"
