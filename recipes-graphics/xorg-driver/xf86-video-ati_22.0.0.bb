require recipes-graphics/xorg-driver/xorg-driver-video.inc

LIC_FILES_CHKSUM = "file://COPYING;md5=aabff1606551f9461ccf567739af63dc"

SUMMARY = "X.Org X server -- ATI Radeon video driver"

DESCRIPTION = "Open-source X.org graphics driver for ATI Radeon graphics"

DEPENDS += "virtual/libx11 libxvmc ${@bb.utils.contains('DISTRO_FEATURES', 'opengl', 'drm', '', d)} \
            xorgproto libpciaccess"

SRC_URI[sha256sum] = "c8c8bb56d3f6227c97e59c3a3c85a25133584ceb82ab5bc05a902a743ab7bf6d"
XORG_DRIVER_COMPRESSOR = ".tar.xz"

EXTRA_OECONF += "${@bb.utils.contains('DISTRO_FEATURES', 'opengl', '', '--disable-glamor', d)}"

RDEPENDS:${PN} += "xserver-xorg-module-exa"
RRECOMMENDS:${PN} += "linux-firmware-radeon"

FILES:${PN} += "${datadir}/X11"
