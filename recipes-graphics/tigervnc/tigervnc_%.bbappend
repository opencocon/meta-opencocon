FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

# for OpenCocon : devide client package

FILES:${PN}-vncviewer = "${bindir}/vncviewer \
                         ${datadir}/applications \
                         ${datadir}/icons \
                         ${datadir}/locale \
"

PACKAGES =+ "${PN}-vncviewer"
