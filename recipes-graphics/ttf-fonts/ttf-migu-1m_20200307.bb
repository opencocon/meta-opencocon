require recipes-graphics/ttf-fonts/ttf.inc

DESCRIPTION = "Migu 1M - Japanese TrueType fonts"
HOMEPAGE = "https://itouhiro.github.io/mixfont-mplus-ipa/migu/"
LICENSE = "IPA"
RDEPENDS:${PN} = "fontconfig-utils"

SRC_URI = "https://github.com/itouhiro/mixfont-mplus-ipa/releases/download/v2020.0307/migu-1m-${PV}.zip"
S = "${WORKDIR}/migu-1m-${PV}"

do_install() {
        install -d ${D}${datadir}/fonts/ttf/
        install -m 0644 ${S}/migu-1m-regular.ttf ${D}${datadir}/fonts/ttf/migu-1m-regular.ttf
}

FILES:${PN} += "${datadir}"

LIC_FILES_CHKSUM = "file://ipag00303/IPA_Font_License_Agreement_v1.0.txt;md5=6cd3351ba979cf9db1fad644e8221276"

SRC_URI[sha256sum] = "e4806d297e59a7f9c235b0079b2819f44b8620d4365a8955cb612c9ff5809321"
