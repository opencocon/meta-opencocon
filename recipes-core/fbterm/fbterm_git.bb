DESCRIPTION = "framebuffer based terminal emulator for linux"
LICENSE = "GPL-2.0-only"

DEPENDS += "freetype fontconfig ncurses-native ncurses"
RDEPENDS:${PN} = "ncurses-terminfo-base"
RRECOMMENDS:${PN} = "ncurses-terminfo"

inherit autotools pkgconfig

SRC_URI = "\
	 git://salsa.debian.org/debian/fbterm.git;protocol=https;branch=master \
         file://206df42bb9026a93fecf3ed515e90fe5b21533e7.patch \
         file://vesadev.patch \
"

SRC_URI:append:libc-musl = "file://c899cbcd131e9ffb17a0e72e870e916a53b3c35e.patch \
                             file://musl-patch-add.patch \
"

LIC_FILES_CHKSUM = "file://COPYING;md5=d8e20eece214df8ef953ed5857862150"

SRCREV = "9c0bbf320e478c29799f9b420fd0aac5951c84a5"

S = "${WORKDIR}/git"



EXTRA_OEMAKE = "\
    'CXXFLAGS=${CXXFLAGS} -I${S}/src/lib' \
    'LDFLAGS=${LDFLAGS} -I${S}/src/lib' \
"


do_configure:prepend() {
  touch ${S}/README
  sed -i -e "s:tic fbterm::g" ${S}/terminfo/Makefile.am
}

