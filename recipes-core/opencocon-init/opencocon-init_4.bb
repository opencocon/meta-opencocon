DESCRIPTION = "opencocon-local init"
SECTION = "base"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"
DEPENDS = "base-files"
RDEPENDS:${PN} = "busybox"

SRC_URI = "file://cocon-poweroff \
           file://default.cnf \
           file://cocon-version \
           file://services \
           file://make-tmpdir \
           file://read-args \
           file://set-hostname \
           file://set-keymap \
           file://sqs-netboot \
           file://set-region \
           file://set-timezone \
           file://swap \
           file://zram \
"

S="${WORKDIR}"

do_install() {
	set -ex

        install -d ${D}${sysconfdir}
        #install -d ${D}${sysconfdir}/default
        install -m 0644 ${WORKDIR}/cocon-version ${D}${sysconfdir}/cocon-version
        install -m 0644 ${WORKDIR}/services ${D}${sysconfdir}/services

	install -d ${D}${datadir}/cocon/
        install -m 0644 ${WORKDIR}/default.cnf ${D}${datadir}/cocon/default.cnf

	install -d ${D}${sysconfdir}/init.d
        # cocon-poweroff : rename
	install -m 0755	${WORKDIR}/cocon-poweroff	${D}${sysconfdir}/init.d/cocon-poweroff.sh
	install -m 0755	${WORKDIR}/make-tmpdir		${D}${sysconfdir}/init.d/make-tmpdir
	install -m 0755 ${WORKDIR}/read-args		${D}${sysconfdir}/init.d/read-args
	install -m 0755 ${WORKDIR}/set-hostname		${D}${sysconfdir}/init.d/set-hostname
	install -m 0755 ${WORKDIR}/set-keymap		${D}${sysconfdir}/init.d/set-keymap
	install -m 0755 ${WORKDIR}/set-region		${D}${sysconfdir}/init.d/set-region
	install -m 0755 ${WORKDIR}/set-timezone		${D}${sysconfdir}/init.d/set-timezone
	install -m 0755	${WORKDIR}/sqs-netboot		${D}${sysconfdir}/init.d/sqs-netboot
	install -m 0755	${WORKDIR}/swap			${D}${sysconfdir}/init.d/swap
	install -m 0755	${WORKDIR}/zram			${D}${sysconfdir}/init.d/zram
}

PACKAGES =+ "${PN}-poweroff ${PN}-make-tmpdir ${PN}-read-args ${PN}-set-hostname ${PN}-set-keymap ${PN}-set-region ${PN}-set-timezone ${PN}-sqs-netboot ${PN}-swap ${PN}-zram"

FILES:${PN} = "${sysconfdir}/cocon-version \
	${sysconfdir}/services \
	${datadir}/cocon/default.cnf \
"
FILES:${PN}-poweroff = "${sysconfdir}/init.d/cocon-poweroff.sh"
FILES:${PN}-make-tmpdir = "${sysconfdir}/init.d/make-tmpdir"
FILES:${PN}-read-args = "${sysconfdir}/init.d/read-args"
FILES:${PN}-set-hostname = "${sysconfdir}/init.d/set-hostname"
FILES:${PN}-set-keymap = "${sysconfdir}/init.d/set-keymap"
FILES:${PN}-set-region = "${sysconfdir}/init.d/set-region"
FILES:${PN}-set-timezone = "${sysconfdir}/init.d/set-timezone"
FILES:${PN}-sqs-netboot = "${sysconfdir}/init.d/sqs-netboot"
FILES:${PN}-swap = "${sysconfdir}/init.d/swap"
FILES:${PN}-zram = "${sysconfdir}/init.d/zram"

inherit update-rc.d

INITSCRIPT_PACKAGES = "${PN}-poweroff ${PN}-make-tmpdir ${PN}-read-args ${PN}-set-hostname ${PN}-set-keymap ${PN}-set-region ${PN}-set-timezone ${PN}-sqs-netboot ${PN}-swap ${PN}-zram"
INITSCRIPT_NAME:${PN}-poweroff = "cocon-poweroff.sh"
INITSCRIPT_PARAMS:${PN}-poweroff = "stop 99 0 6 ."
INITSCRIPT_NAME:${PN}-make-tmpdir = "make-tmpdir"
INITSCRIPT_PARAMS:${PN}-make-tmpdir = "start 00 S ."
INITSCRIPT_NAME:${PN}-read-args = "read-args"
INITSCRIPT_PARAMS:${PN}-read-args = "start 00 S ."
INITSCRIPT_NAME:${PN}-set-hostname = "set-hostname"
INITSCRIPT_PARAMS:${PN}-set-hostname = "start 50 S ."
INITSCRIPT_NAME:${PN}-set-keymap = "set-keymap"
INITSCRIPT_PARAMS:${PN}-set-keymap = "start 04 5 ."
INITSCRIPT_NAME:${PN}-set-region = "set-region"
INITSCRIPT_PARAMS:${PN}-set-region = "start 03 5 ."
INITSCRIPT_NAME:${PN}-set-timezone = "set-timezone"
INITSCRIPT_PARAMS:${PN}-set-timezone = "start 01 5 ."
INITSCRIPT_NAME:${PN}-sqs-netboot = "sqs-netboot"
INITSCRIPT_PARAMS:${PN}-sqs-netboot = "start 00 S ."
INITSCRIPT_NAME:${PN}-swap = "swap"
INITSCRIPT_PARAMS:${PN}-swap = "start 03 S ."
INITSCRIPT_NAME:${PN}-zram = "zram"
INITSCRIPT_PARAMS:${PN}-zram = "start 06 S ."

