# Released under the MIT license (see COPYING.MIT for the terms)

DESCRIPTION = "Packagegroup for OpenCocon thinclient distribution"

#LICENSE = "MIT"
#LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit packagegroup


RDEPENDS:${PN} = " \
alsa-utils-alsactl \
alsa-utils-alsamixer \
alsa-utils-amixer \
alsa-utils-aplay \
alsa-utils-speakertest \
base-files \
base-passwd \
busybox \
cocon-data \
cocon-data-doc \
cocon-thinclient-desktop \
coconcnf \
coconcnf-scancnf \
coconset \
consolekit \
dbus-x11 \
dialog \
encodings \
font-alias \
font-util \
freerdp \
gnome-icon-theme \
hicolor-icon-theme \
inputattach \
iproute2 \
liberation-fonts \
libgcrypt \
libgpg-error \
lxpanel \
lxrandr \
matchbox-wm \
mkfontdir \
mlterm \
netsurf \
network-manager-applet \
networkmanager \
networkmanager-nmtui \
nss \
ntpdate \
opencocon-init \
opencocon-init-make-tmpdir \
opencocon-init-poweroff \
opencocon-init-read-args \
opencocon-init-set-hostname \
opencocon-init-set-keymap \
opencocon-init-set-region \
opencocon-init-set-timezone \
opencocon-init-swap \
opencocon-init-zram \
openssl \
opkg \
rfkill \
setxkbmap \
spice-gtk \
sudo \
tigervnc-vncviewer \
ttf-bitstream-vera \
ttf-migu-1m \
tzdata \
udev \
update-rc.d \
usbutils \
util-linux-cfdisk \
util-linux-fdisk \
wpa-supplicant \
x11perf \
xauth \
xdpyinfo \
xinit \
xinput-calibrator \
xmodmap \
xorg-minimal-fonts \
xpra \
xrandr \
xserver-common \
xserver-xf86-config \
xserver-xorg \
xserver-xorg-xephyr \
xuser-account \
"

# TODO : pyhoca-cli coconvnc libvncviewer
