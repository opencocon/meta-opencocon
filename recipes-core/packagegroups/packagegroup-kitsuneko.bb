# Released under the MIT license (see COPYING.MIT for the terms)

DESCRIPTION = "Packagegroup for Kitsuneko distribution"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit packagegroup

RDEPENDS:${PN} = " \
alsa-utils-alsactl \
alsa-utils-alsamixer \
alsa-utils-amixer \
alsa-utils-aplay \
alsa-utils-speakertest \
coconset \
networkmanager \
fbterm \
ttf-koruri \
ttf-migu-1m \
dmidecode \
nanotodon \
netsurf \
gdb \
gdbserver \
wmbubble \
opkg \
opencocon-init \
opencocon-init-make-tmpdir \
opencocon-init-read-args \
opencocon-init-set-hostname \
opencocon-init-set-timezone \
opencocon-init-swap \
opencocon-init-zram \
rfkill \
udev \
update-rc.d \
mlterm \
icewm \
freerdp \
${@bb.utils.contains('DISTRO_FEATURES', 'opengl', 'atlantis raphael', '', d)} \
"
