# look for files in the layer first
FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SYSVINIT_ENABLED_GETTYS = "1 2 3"

# for dunfell
do_install:append() {
	for n in ${SYSVINIT_ENABLED_GETTYS}
	do
		echo "tty$n:12345:respawn:${base_sbindir}/getty 38400 tty$n" >> ${D}${sysconfdir}/inittab
	done
	echo "" >> ${D}${sysconfdir}/inittab
}
