# Kitsuneko : R&D image for OpenCocon

inherit core-image

IMAGE_PKGTYPE = "ipk"

IMAGE_NAME = "kitsuneko-${MACHINE}-${TCLIBC}-${DISTRO_VERSION}"

DISTRO_UPDATE_ALTERNATIVES ?= "${PREFERRED_PROVIDER_virtual/update-alternatives}"
DISTRO_PACKAGE_MANAGER ?= "ipkg ipkg-collateral"
ONLINE_PACKAGE_MANAGEMENT ?= "no"

IMAGE_FEATURES = " \
        debug-tweaks \
	"

IMAGE_ROOTFS_EXTRA_SPACE = "32768"

IMAGE_INSTALL = "packagegroup-base \
                 packagegroup-core-boot \
                 packagegroup-core-sdk \
                 packagegroup-core-ssh-dropbear \
                 packagegroup-core-x11-base \
                 packagegroup-firmware \
                 packagegroup-kitsuneko \
                 wireless-regdb-static \
                 "

IMAGE_FSTYPES += "tar.gz tar.gz.md5sum"

IMAGE_GEN_DEBUGFS = "1"
IMAGE_FSTYPES_DEBUGFS = "tar.gz tar.gz.md5sum"
IMAGE_ROOTFS_EXTRA_SPACE = "131072"

IMAGE_FEATURES += "empty-root-password allow-empty-password"
