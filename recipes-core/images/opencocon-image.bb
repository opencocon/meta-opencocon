# opencocon thinclient master image (all plathome)

IMAGE_PKGTYPE = "ipk"


IMAGE_NAME = "opencocon-${DISTRO_VERSION}-${MACHINE}"

# TODO : postprocess?
#IMAGE_PREPROCESS_COMMAND = "create_etc_timestamp"

DISTRO_UPDATE_ALTERNATIVES ?= "${PREFERRED_PROVIDER_virtual/update-alternatives}"

DISTRO_PACKAGE_MANAGER ?= "ipkg ipkg-collateral"
ONLINE_PACKAGE_MANAGEMENT = "no"

# FIXME: We need a distro-indendent way of specifying feed configs.
# Once the RFC for the DISTRO_FEED_CONFIGS variable name is approved,
# we can remove this default definition and set it in the distro config.
#DISTRO_FEED_CONFIGS ?= "${ANGSTROM_FEED_CONFIGS}"


COCON_TC_PACKAGES = " \
packagegroup-base-wifi \
packagegroup-core-boot \
packagegroup-core-x11-base \
packagegroup-firmware \
packagegroup-opencocon \
wireless-regdb-static \
"

RDEPENDS = "${COCON_TC_PACKAGES}"
IMAGE_INSTALL = "${COCON_TC_PACKAGES}"

PACKAGE_EXCLUDE = "linux-firmware \
pulseaudio-module-null-source \
"

IMAGE_BASENAME = "opencocon"
IMAGE_FSTYPES += "tar.gz squashfs"
IMAGE_FEATURES += "empty-root-password allow-empty-password"
IMAGE_ROOTFS_EXTRA_SPACE = "32768"

create_stampfile() {
  echo "${DISTRO}-${DISTRO_VERSION}-${MACHINE}-${TCLIBC}" > ${IMAGE_ROOTFS}/.image_version
}

IMAGE_PREPROCESS_COMMAND += "create_stampfile;"

inherit image
