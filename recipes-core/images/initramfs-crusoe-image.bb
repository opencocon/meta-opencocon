# initramfs image with interactive boot menu allowing to select rootfs location
# from choices of block devices, loopback images and NFS.

#IMAGE_NAME = "${DISTRO_NAME}-${DISTRO_VERSION}-${MACHINE}-crusoe"

PACKAGE_INSTALL = " \
busybox \
initramfs-framework-base \
initramfs-module-udev \
initramfs-module-crusoe \
"

IMAGE_LINGUAS = ""

# crusoe image isn't use rootfs
BAD_RECOMMENDATIONS += "initramfs-module-rootfs busybox-syslog"

export IMAGE_BASENAME = "crusoe"

IMAGE_FSTYPES += "squashfs"

create_stampfile() {
  echo "${DISTRO}-${DISTRO_VERSION}-${MACHINE}-${TCLIBC}" > ${IMAGE_ROOTFS}/.image_version
}

IMAGE_PREPROCESS_COMMAND += "create_stampfile;"

inherit core-image
