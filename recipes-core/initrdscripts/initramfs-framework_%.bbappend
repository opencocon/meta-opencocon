# look for files in the layer first
FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

PR = "r2"

SRC_URI += " \
            file://mountsqs \
            file://pivotroot \
            file://clean \
"

do_install:append() {
	# needed pre-created directory on squashfs
	install -d ${D}/rootfs
	install -d ${D}/proc
	install -d ${D}/sys

	# OpenCocon-Specfic directory
	install -d ${D}/.realroot
	install -d ${D}/.union
	install -d ${D}/.ram
	install -d ${D}/.oldroot
	install -d ${D}/.newroot
	install -d ${D}/.cfg
	install -d ${D}/.copytoram
	install -d ${D}/.iso
	install -d ${D}/.mod

	# OpenCocon-local scripts
	install -m 0755 ${WORKDIR}/mountsqs ${D}/init.d/80-mountsqs
	install -m 0755 ${WORKDIR}/pivotroot ${D}/init.d/99-pivotroot
	install -m 0755 ${WORKDIR}/clean ${D}/clean
	
	# We don't use 99-finish on default initramfs-framework-base. so delete this.
	rm ${D}/init.d/99-finish
}

PACKAGES += "initramfs-module-crusoe"

FILES:${PN}-base += "/rootfs /proc /sys /.realroot /.union /.ram /.oldroot /.newroot /.cfg /.copytoram /.iso /.mod /clean"

SUMMARY:initramfs-module-crusoe = ""
RDEPENDS:initramfs-module-crusoe = "${PN}-base"
FILES:initramfs-module-crusoe = "/init.d/80-mountsqs /init.d/99-pivotroot"

FILES:${PN}-base:remove = "/init.d/99-finish"
