# look for files in the layer first
FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

# We dont use GTK4.
DEPENDS:remove = "gtk4"
EXTRA_OEMESON:remove = "-Dlibnma_gtk4=true"
