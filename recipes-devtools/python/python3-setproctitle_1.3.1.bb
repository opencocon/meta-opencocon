DESCRIPTION = "A Python module to customize the process title"
HOMEPAGE = "https://github.com/dvarrazzo/py-setproctitle"
SECTION = "devel/python"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://COPYRIGHT;md5=86d2d41b5f4f023f43466f8cb7adebaa"

SRC_URI[sha256sum] = "3d134c2effeb945e8227f7d3d24ea8ad49c03c87ac91a8d67bf967730fa9daba"

inherit pypi setuptools3
