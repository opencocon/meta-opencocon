# recipe from meta-wayland, https://github.com/MarkusVolk/meta-wayland/blob/kirkstone/recipes-devtools/python/python-xlib_git.bb

SUMMARY = "The Python X Library."
LICENSE = "LGPL-2.1-only"
LIC_FILES_CHKSUM = "file://LICENSE;md5=fc178bcd425090939a8b634d1d6a9594"

SRC_URI = " \
	git://github.com/python-xlib/python-xlib.git;protocol=https;branch=master \
"

DEPENDS = " \
	python3-setuptools-scm-native \
	python3-wheel-native \
"


RDEPENDS:${PN} = " \
	python3 \
	python3-six \
"

inherit setuptools3 python_pep517

S = "${WORKDIR}/git"
SRCREV = "c87624dd6ec780417e2d32529976d65fed344045"
