NETSURF_VER = "3.11"

SRC_URI = "https://download.netsurf-browser.org/netsurf/releases/source-full/netsurf-all-${NETSURF_VER}.tar.gz"
SRC_URI[sha256sum] = "4dea880ff3c2f698bfd62c982b259340f9abcd7f67e6c8eb2b32c61f71644b7b"
