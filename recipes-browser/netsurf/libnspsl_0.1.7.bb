require recipes-browser/netsurf/netsurf.inc
require recipes-browser/netsurf/netsurf-lib.inc

DEPENDS:append = "bison-native libtie-ixhash-perl-native"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=b34d4c38795ec2c105824d4a85ab57ca"

PR = "r2"
