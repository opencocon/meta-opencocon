require recipes-browser/netsurf/netsurf.inc
require recipes-browser/netsurf/netsurf-lib.inc

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=52bc35b9173484ab8ece31722b91336b"

DEPENDS:append = "libdom libwapcaplet gperf-native"
