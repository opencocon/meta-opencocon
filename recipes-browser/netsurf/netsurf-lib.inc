DEPENDS = "netsurf-buildsystem-native "

S = "${WORKDIR}/netsurf-all-${NETSURF_VER}/${PN}"

do_configure() {
        PREFIX="${D}/usr" NSSHARED="${RECIPE_SYSROOT_NATIVE}/${datadir}/netsurf-buildsystem" COMPONENT_TYPE=lib-shared oe_runmake clean
}

do_compile() {
	PREFIX="${D}/usr" NSSHARED="${RECIPE_SYSROOT_NATIVE}/${datadir}/netsurf-buildsystem" COMPONENT_TYPE=lib-shared oe_runmake
}

do_install() {
 	PREFIX="${D}/usr" NSSHARED="${RECIPE_SYSROOT_NATIVE}/${datadir}/netsurf-buildsystem" COMPONENT_TYPE=lib-shared oe_runmake install

	# pkgconfig : QA
	sed -i -e "s:${D}::g" ${D}/${libdir}/pkgconfig/${PN}.pc
}

inherit pkgconfig
