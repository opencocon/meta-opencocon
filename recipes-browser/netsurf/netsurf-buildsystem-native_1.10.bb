require recipes-browser/netsurf/netsurf.inc

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=4d0f1234b95ab141178c0f1454f6cc38"

S = "${WORKDIR}/netsurf-all-${NETSURF_VER}/buildsystem"

do_install() {
  install -d ${D}${datadir}/netsurf-buildsystem/
  install -d ${D}${datadir}/netsurf-buildsystem/makefiles/
  install -m 0644 ${S}/makefiles/Makefile.top ${D}${datadir}/netsurf-buildsystem/makefiles/Makefile.top
  install -m 0644 ${S}/makefiles/Makefile.top ${D}${datadir}/netsurf-buildsystem/makefiles/Makefile.top
  install -m 0644 ${S}/makefiles/Makefile.gcc ${D}${datadir}/netsurf-buildsystem/makefiles/Makefile.gcc
  install -m 0644 ${S}/makefiles/Makefile.pkgconfig ${D}${datadir}/netsurf-buildsystem/makefiles/Makefile.pkgconfig
  install -m 0644 ${S}/makefiles/Makefile.subdir ${D}${datadir}/netsurf-buildsystem/makefiles/Makefile.subdir
  install -m 0644 ${S}/makefiles/Makefile.tools ${D}${datadir}/netsurf-buildsystem/makefiles/Makefile.tools
}

inherit native
