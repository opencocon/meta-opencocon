require recipes-browser/netsurf/netsurf.inc

LICENSE = "GPL-2.0-only | MIT"
LIC_FILES_CHKSUM = "file://../COPYING;md5=18f143cd9285671a20df639a0cd06c99"

DEPENDS = "zlib-native libpng-native"

S = "${WORKDIR}/netsurf-all-${NETSURF_VER}/netsurf/tools"

do_compile() {
  ${CC} ${CFLAGS} ${LDFLAGS} -I ${WORKDIR}/netsurf-all-${NETSURF_VER}/netsurf xxd.c -o xxd
  ${CC} ${CFLAGS} ${LDFLAGS} -I ${WORKDIR}/netsurf-all-${NETSURF_VER}/netsurf split-messages.c -o split-messages -lz
  ${CC} ${CFLAGS} ${LDFLAGS} -I ${WORKDIR}/netsurf-all-${NETSURF_VER}/netsurf convert_image.c -o convert_image $(pkg-config --cflags --libs libpng)
  ${CC} ${CFLAGS} ${LDFLAGS} -I ${WORKDIR}/netsurf-all-${NETSURF_VER}/netsurf convert_font.c -o convert_font
}


do_install() {
  install -d ${D}${bindir}/
  install -m 0755 ${S}/xxd ${D}${bindir}/netsurf-xxd
  install -m 0755 ${S}/split-messages ${D}${bindir}/netsurf-split-messages
  install -m 0755 ${S}/convert_image ${D}${bindir}/netsurf-convert_image
  install -m 0755 ${S}/convert_font ${D}${bindir}/netsurf-convert_font
}

inherit pkgconfig native
