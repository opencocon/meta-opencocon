require recipes-browser/netsurf/netsurf.inc
require recipes-browser/netsurf/netsurf-lib.inc

DEPENDS:append = "bison-native"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=1331196edcf4fe5bcfb2df05db4f9263"
