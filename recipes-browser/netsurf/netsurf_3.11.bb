require recipes-browser/netsurf/netsurf.inc

LICENSE = "GPL-2.0-only | MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=18f143cd9285671a20df639a0cd06c99"

S = "${WORKDIR}/netsurf-all-${PV}/${PN}"

SRC_URI:append = " \
	file://tools-is-precompiled.patch \
"

DEPENDS = "libcss libdom libhubbub libnsutils libutf8proc libnsbmp libnsgif nsgenbind-native libnspsl librosprite libsvgtiny netsurf-buildsystem-native \
           libnslog libxml2 curl gtk+3 jpeg libmng libpng libwebp glib-2.0-native gdk-pixbuf-native vim-native libhtml-parser-perl-native netsurf-tools-native"
RDEPENDS:${PN} = "nsgenbind"

do_configure() {
        oe_runmake clean

	# Generate Makefile.config
	cat > ${S}/Makefile.config <<EOF
COMPONENT_TYPE=binary
TARGET=gtk3
EOF

	sed -i -e "s:BUILD_CC \:= cc:BUILD_CC \:= ${CC}:g" ${S}/Makefile
}


do_compile() {
	# copy precompiled tools, built by netsurf-tools-native
	mkdir -p ${S}/build/Linux-gtk3/tools/
	cp ${RECIPE_SYSROOT_NATIVE}${bindir}/netsurf-xxd ${S}/build/Linux-gtk3/tools/xxd
	cp ${RECIPE_SYSROOT_NATIVE}${bindir}/netsurf-split-messages ${S}/build/Linux-gtk3/tools/split-messages
	cp ${RECIPE_SYSROOT_NATIVE}${bindir}/netsurf-convert_image ${S}/build/Linux-gtk3/tools/convert_image
	cp ${RECIPE_SYSROOT_NATIVE}${bindir}/netsurf-convert_font ${S}/build/Linux-gtk3/tools/convert_font

        PREFIX="${D}/usr" NSSHARED="${RECIPE_SYSROOT_NATIVE}/${datadir}/netsurf-buildsystem" oe_runmake
}

do_install() {
        PREFIX="${D}/usr" NSSHARED="${RECIPE_SYSROOT_NATIVE}/${datadir}/netsurf-buildsystem" oe_runmake install
	chown root:root -R ${D}${datadir}/netsurf/
}

inherit pkgconfig 
