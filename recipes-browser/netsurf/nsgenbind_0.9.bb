require recipes-browser/netsurf/netsurf.inc

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=7d62c723085cb9411630e235813cc40d"

DEPENDS = "netsurf-buildsystem-native bison-native"

S = "${WORKDIR}/netsurf-all-${NETSURF_VER}/nsgenbind"
NSSHARED:class-target = "${RECIPE_SYSROOT_NATIVE}/${datadir}/netsurf-buildsystem"
NSSHARED:class-native = "${datadir}/netsurf-buildsystem"

do_configure() {
        PREFIX="${D}/usr" NSSHARED="${NSSHARED}" COMPONENT_TYPE=binary oe_runmake clean
}

do_compile() {
        PREFIX="${D}/usr" NSSHARED="${NSSHARED}" COMPONENT_TYPE=binary oe_runmake
}

do_install() {
#        PREFIX="${D}/usr" NSSHARED="${NSSHARED}" COMPONENT_TYPE=binary oe_runmake install
	install -d ${D}${bindir}/
	install -m 0755 ${S}/`ls | grep build`/nsgenbind ${D}${bindir}/nsgenbind
}

BBCLASSEXTEND = "native"
