require recipes-browser/netsurf/netsurf.inc
require recipes-browser/netsurf/netsurf-lib.inc

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=cf7b66fee3121ea916ec80ea35314e19"

DEPENDS:append = "libwapcaplet libparserutils expat libhubbub"
