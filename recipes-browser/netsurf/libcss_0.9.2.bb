require recipes-browser/netsurf/netsurf.inc
require recipes-browser/netsurf/netsurf-lib.inc

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=cf7b66fee3121ea916ec80ea35314e19"

DEPENDS:append = "libwapcaplet libparserutils"

do_configure:append() {
	# Wordaround from Gentoo
	sed -e '1i#pragma GCC diagnostic ignored "-Wimplicit-fallthrough"' -i ${S}/src/parse/parse.c
	sed -e '1i#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"' -i ${S}/src/parse/parse.c
	sed -e '1i#pragma GCC diagnostic ignored "-Wimplicit-fallthrough"' -i ${S}/src/select/arena_hash.h
	sed -e '1i#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"' -i ${S}/src/select/computed.c
}
