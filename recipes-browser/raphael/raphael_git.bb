DESCRIPTION = "Raphael is a fork of the Midori web browser"
LICENSE = "LGPL-2.1-or-later"
LIC_FILES_CHKSUM = "file://COPYING;md5=fbc093901857fcd118f065f900982c24"
DEPENDS = "webkitgtk libsoup-2.4 glib-2.0 gettext libxml2 openssl sqlite3 gcr libpeas json-glib libarchive vala-native librsvg-native intltool-native gobject-introspection-native"

SRCREV = "c94d75a2bd8bf49ee18d9aceb93ca2617ec3a0cb"
SRC_URI = "git://github.com/MidnightBSD/raphael.git;branch=master;protocol=https \
"
S = "${WORKDIR}/git"

inherit gtk-icon-cache pkgconfig cmake vala mime-xdg

#EXTRA_OECMAKE_BUILD = " -j 1 "
EXTRA_OECMAKE = " -DGIR_COMPILER_BIN=${RECIPE_SYSROOT_NATIVE}/${bindir}/g-ir-compiler "

#do_install() {
#    oe_runmake DESTDIR=${D} install
#    rmdir ${D}${datadir}/gir-1.0
#}

RRECOMMENDS:${PN} += "glib-networking ca-certificates gnome-icon-theme"
FILES:${PN} += "${datadir}/metainfo/* \
                ${libdir}/girepository-1.0/* \
"
FILES:${PN}-dev += "${datadir}/vala/vapi"
