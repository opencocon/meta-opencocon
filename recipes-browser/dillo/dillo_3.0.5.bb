# from meta-webstuff, https://github.com/kernelconcepts/meta-webstuff

DESCRIPTION = "Lightweight FLTK browser, with support for SSL, tabs and much more..."
HOMEPAGE = "http://www.dillo.org"
SECTION = "x11/network"
PRIORITY = "optional"
LICENSE = "GPL-3.0-only"

LIC_FILES_CHKSUM = "file://${S}/COPYING;md5=d32239bcb673463ab874e80d47fae504"

DEPENDS = "fltk libpng openssl jpeg"

SRC_URI = "http://www.dillo.org/download/dillo-${PV}.tar.bz2 \
           file://dillo2-inbuf.patch \
           file://dillo-3.0.5-fno-common.patch \
           file://dillo-3.0.5-openssl-1.1.patch \
           file://configure_ac.patch \
"

inherit autotools pkgconfig

FILES:${PN} += " ${libdir}/dillo/ ${bindir}/dpi* "
FILES:${PN}-dbg += " ${libdir}/dillo/dpi/*/.debug/"

SRC_URI[sha256sum] = "db1be16c1c5842ebe07b419aa7c6ef11a45603a75df2877f99635f4f8345148b"

EXTRA_OECONF = "--enable-ipv6 --enable-ssl"

#do_configure() {
#	sed -i 's:-I/usr/local/include::g' ${S}/configure*
#	sed -i 's:-L/usr/local/lib::g' ${S}/configure*
#	PNG_CONFIG=pkg-config oe_runconf
#}

#do_compile() {
#	PNG_CONFIG=pkg-config oe_runmake
#}

do_install:append() {
        install -d ${D}${datadir}/applications
        install -d ${D}${datadir}/pixmaps
}
