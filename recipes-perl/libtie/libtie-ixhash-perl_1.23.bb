SUMMARY = "ordered associative arrays for Perl"
SECTION = "libs"
LICENSE = "Artistic-1.0 | GPL-1.0-or-later"

LIC_FILES_CHKSUM = "file://lib/Tie/IxHash.pm;beginline=637;endline=639;md5=b131dcfe180200c34ccd6e88c3288814"
SRC_URI = "https://cpan.metacpan.org/authors/id/C/CH/CHORNY/Tie-IxHash-${PV}.tar.gz"

S = "${WORKDIR}/Tie-IxHash-${PV}"

inherit cpan

SRC_URI[sha256sum] = "fabb0b8c97e67c9b34b6cc18ed66f6c5e01c55b257dcf007555e0b027d4caf56"

BBCLASSEXTEND="native"
