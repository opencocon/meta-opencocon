# from meta-lxde https://git.toradex.com/cgit/meta-lxde.git/

SUMMARY = "LXDE Panel"
HOMEPAGE = "http://lxde.org/"
SECTION = "x11"

LICENSE = "GPL-2.0-or-later"
LIC_FILES_CHKSUM = "file://COPYING;md5=9d19a9495cc76dc96b703fb4aa157183"

DEPENDS = "alsa-lib glib-2.0-native gtk+3 intltool-native keybinder-3.0 libwnck3 libxml2 libxmu libxpm lxmenu-data menu-cache libfm"

SRC_URI = "${SOURCEFORGE_MIRROR}/lxde/lxpanel-${PV}.tar.xz \
           file://lxpanel-broken-apps.patch \
           file://create_target_dir.patch \
           file://0001-panel.c-gtk3-set-minimum-geometry.patch \
"
SRC_URI[sha256sum] = "1e318f57d7e36b61c23a504d03d2430c78dad142c1804451061f1b3ea5441ee8"

inherit autotools gettext pkgconfig

# for OpenCocon distribution
EXTRA_OECONF += "--enable-alsa --enable-gtk3 --with-x --with-plugins=volume,cpu,xkb"

FILES:${PN}-dbg += "${libdir}/lxpanel/plugins/.debug"
FILES:${PN}-dev += "${libdir}/lxpanel/*.so"
