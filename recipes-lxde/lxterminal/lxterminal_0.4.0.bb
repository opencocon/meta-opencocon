# from meta-lxde https://git.toradex.com/cgit/meta-lxde.git/

SUMMARY = "Lightweight vte-based tabbed terminal emulator for LXDE"
HOMEPAGE = "http://lxde.sf.net"
SECTION = "x11"
LICENSE = "GPL-2.0-or-later"
LIC_FILES_CHKSUM = "file://COPYING;md5=59530bdf33659b29e73d4adb9f9f6552"

DEPENDS = "glib-2.0 glib-2.0-native gtk+3 intltool-native vte xmlto-native"

SRC_URI = " \
    ${SOURCEFORGE_MIRROR}/lxde/lxterminal-${PV}.tar.xz \
    file://0002-man-Makefile.am-don-t-error-out-on-missing-man-depen.patch \
"
SRC_URI[sha256sum] = "9db8748923b3fa09a82ae2210ed1fa4cdb4c45312009da9caed103d48f8e9be7"

EXTRA_OECONF += "--enable-gtk3 --enable-man"

FILES:${PN} += "${datadir}/icons/hicolor/128x128/apps/lxterminal.png"

inherit autotools pkgconfig gettext

