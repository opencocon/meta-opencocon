DESCRIPTION = "OpenCocon config reader"
LICENSE = "MIT"

DEPENDS = "libinih libconfig"

inherit autotools pkgconfig update-rc.d

SRC_URI = "\
	git://gitlab.com/opencocon/coconcnf.git;protocol=https;branch=main \
	file://scancnf \
"

LIC_FILES_CHKSUM = "file://LICENSE;md5=3310122a6e668518eaf490a816b4c1c8"

SRCREV = "f6a2bbac17d62ed719793ddce8f415a0dfba05a4"

S = "${WORKDIR}/git"

do_install:append() {
  set -ex

  install -d ${D}${sysconfdir}/init.d
  install -m 0755 ${WORKDIR}/scancnf ${D}${sysconfdir}/init.d/scancnf
}

PACKAGES =+ "${PN}-scancnf"

FILES:${PN}-scancnf = "${sysconfdir}/init.d/scancnf"
 
INITSCRIPT_PACKAGES = "${PN}-scancnf"
INITSCRIPT_NAME:${PN}-scancnf = "scancnf"
INITSCRIPT_PARAMS:${PN}-scancnf = "start 00 5 ."
