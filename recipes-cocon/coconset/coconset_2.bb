DESCRIPTION = "Generate various OpenCocon dialogs"
RDEPENDS:${PN} = "dialog"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

DEPENDS = "libxml2-native xkeyboard-config tzdata"


SRC_URI = "file://generate-region-select.sh \
           file://parse-keymap.sh \
           file://generate-keymap-select.sh \
           file://generate-timezone-select.sh"

S="${WORKDIR}"

do_compile() {
  cd ${S}
  
  chmod +x generate-region-select.sh parse-keymap.sh generate-keymap-select.sh generate-timezone-select.sh

  # Generate region select
  ./generate-region-select.sh ${RECIPE_SYSROOT}/${datadir}/zoneinfo/ > ./cocon-region-select

  # Generate keymap select
  ./generate-keymap-select.sh ${RECIPE_SYSROOT}/${datadir}/X11/xkb/rules/ ${RECIPE_SYSROOT_NATIVE}/${bindir} > ./cocon-keymap-select

  # Generate timezone select
  ./generate-timezone-select.sh ${RECIPE_SYSROOT}/${datadir}/zoneinfo/posix/ > ./cocon-timezone-select
}


do_install() {
  set -ex

  install -d ${D}${bindir}/
  install -m 0755 ${WORKDIR}/cocon-region-select ${D}${bindir}/cocon-region-select
  install -m 0755 ${WORKDIR}/cocon-keymap-select ${D}${bindir}/cocon-keymap-select
  install -m 0755 ${WORKDIR}/cocon-timezone-select ${D}${bindir}/cocon-timezone-select
}

FILES:${PN} = "/"

