#!/bin/sh

# Generate keyboard layout dialog

echo "#!/bin/sh"
echo "dialog --clear --nocancel --no-kill --default-item jp --menu \"Select keyboard layout :\" 25 45 25 \\"

./parse-keymap.sh "${1}" "${2}" | sort

echo "2>/tmp/.cocon.keymap"
