#!/bin/sh

# Generates Region select dialog

echo "#!/bin/sh"
echo "dialog --clear --nocancel --no-kill --default-item \"Asia/Tokyo\" --menu \"Select timezone :\" 25 55 30 \\"
IFS='	'


find "${1}." -name '*' -type f | sed -e "s|${1}\.\/||g" | while read -r a b
do
	echo "\"${a}\" \"${a}\" \\"
done

echo "2>/tmp/.cocon.timezone"

