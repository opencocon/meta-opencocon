#!/bin/sh

# fetch from keyboard layout database using xmllint

name=''

"${2}/xmllint" --xpath "/xkbConfigRegistry/layoutList/layout/configItem/name | xkbConfigRegistry/layoutList/layout/configItem/description" "${1}/base.xml" | sed -e "s|N'Ko|N\\\'Ko|g" | while read a
do
	if echo "${a}" | grep -q 'custom'
	then
		continue;
	fi

	if [ "$( echo "${a}" | grep -c '<name>' )" -eq 1 ];
	then
		name=$( echo "${a}" | sed -e 's/<[^>]*>//g' )
	else
		echo "${name} \"$( echo "${a}" | sed -e 's/<[^>]*>//g' )\" \\"
	fi
done

