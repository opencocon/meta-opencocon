#!/bin/sh

# Generates Region select dialog

echo "#!/bin/sh"
echo "dialog --clear --nocancel --no-kill --default-item JP --menu \"Select your region :\" 25 45 25 \\"
IFS='	'

sed -e '/^$/d' -e '/\#/d' "${1}/iso3166.tab" | while read -r a b
do
	echo "${a} \"${b}\" \\"
done

echo "2>/tmp/.cocon.wifireg"

